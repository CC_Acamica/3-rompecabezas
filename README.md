**Tercer proyecto de la carrera Desarrollo Web Full Stack de Acámica. El objetivo es aprender en profundidad y de manera comprensiva los fundamentos de la programación Front End.**

---

## Objetivo del proyecto
Este proyecto consiste en crear un juego rompecabezas que funcione online y se ejecute en un navegador.

Crear una página web dinámica y con movimiento a demanda de los usuarios es una manera de aprender en profundidad y de manera comprensiva los fundamentos de la programación Front End. A medida que avanzás en la construcción de tu propio juego online, vas a entender cómo responde la computadora a tus órdenes.

Con lo que aprendas en este proyecto vas a poder crear aplicaciones web con diferentes comportamientos: desde movimientos dinámicos en pantalla a partir de que el usuario apriete una tecla, hasta mostrar carteles automáticamente desencadenados por una acción específica. Además vas a aprender más sobre CSS para hacer tus páginas atractivas e interesantes.

---

## Proyecto finalizado
El proyecto finalizado se encuentra en el directorio ChristianCabrer-Rompecabezas.
Se puede ejecutar el mismo abriendo el archivo juego.html.

---

## Descripción de como se llevo a cabo el proyecto
* Primero le di todo el estilo que deseaba al juego, modificando las fuentes, tamaños de letra y colores.
* Después me dedique al JS completando todas las funcionalidades del rompecabezas: Mostrar rompecabezas inicial y desordenarlo, detectar las teclas presionadas por el usuario, movimiento de las piezas, detectar cuando se gana el juego, mostrar cartel ganador.
* Para el mensaje ganador utilice el plugin "https://sweetalert2.github.io/" para mostrar un mensaje mas amigable con el usuario y poder darle el comportamiento deseado de una manera sensilla.
* Una vez resuelto todo el funcionamiento del rompecabezas agregue un 2do nivel con 16 fichas en lugar de 9 y otra imagen. Que funcionan con 2 nuevos archivos "juego2.html" y "juego_Nivel2.js".

---